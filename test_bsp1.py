import unittest
import bsp1

class TestBsp1(unittest.TestCase):

    def test_addNum(self):
        self.assertEqual(bsp1.addNum(10, 2), 12)
        self.assertEqual(bsp1.addNum(12, 2), 14)
        # etc...

    def test_subNum(self):
        self.assertEqual(bsp1.multNum(2, 2), 4)
        # etc...

    def test_divNum(self):
        self.assertEqual(bsp1.divNum(2, 2), 1)
        # etc...

        # Es gibt verschiedene "assert..." statements:
        self.assertRaises(ValueError, bsp1.divNum, 2, 0)
        # (siehe hierzu Dokumentation)
