# Unit Test: Einfaches Beispiel

def addNum(x, y):
    return x + y

def subNum(x, y):
    return x - y

def divNum(x, y):
    if y == 0:
        raise ValueError("Come on man, what are you doing?")
    return x / y

def multNum(x, y):
    return x * y

def modNum(x, y):
    return x % y

def run():
    result = divNum(subNum(2, 3), multNum(2, 3))
